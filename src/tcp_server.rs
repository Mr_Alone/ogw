use std::net::TcpListener;
use std::io::{Read, Write};
use std::str;

use login::Login;
use maps::Maps;
use session::Session;
use consts::{BUF_SIZE, SEPARATOR};

pub fn get_id(login: &mut Login, request: &str) -> Result<usize, ()>
{
    if request.is_empty() {
        return Err(());
    }
    let argv: Vec<&str> = request.split(SEPARATOR).collect();
    match argv.len() {
        1 => {
            let user_name = argv[0];
            Ok(login.get_new_session(user_name))
        },
        2 => {
            let user_name = argv[0];
            let friend_name = argv[1];
            Ok(login.get_attached_session(user_name, friend_name))
        },
        _ => Err(())
    }
}

pub fn process(endpoint : &str) {
    let mut login = Login::new();
    let mut maps = Maps::new();

    let listener = TcpListener::bind(endpoint).unwrap();
    let mut buf = [0; BUF_SIZE];
    for stream in listener.incoming() {
        let mut stream = stream.unwrap();
        let size = stream.read(&mut buf).unwrap();
        let buf = str::from_utf8(&buf[..size]).unwrap();
        match get_id(&mut login, &buf) {
            Ok(id) => {
                let session = Session::new(id, maps.get_map(&id));
                stream.write(session.to_string().as_bytes()).unwrap();
            },
            Err(_) => break
        };
    }
}
