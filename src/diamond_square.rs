use rand::{Rng, SeedableRng, StdRng};

use point::Point;

struct DiamondSquare {
    size: usize,
    full_size: usize,
    height: u8,
    rng: StdRng,
}

impl DiamondSquare {
    pub fn new(size: usize, height: u8, map_id: usize) -> DiamondSquare {
        let seed: &[_] = &[map_id];
        DiamondSquare {
            size: size,
            full_size: size * size,
            height: height,
            rng: SeedableRng::from_seed(seed),
        }
    }

    pub fn gen(&mut self) -> Vec<u8> {
        let mut arr = self.init();
        let tl = Point::new(0, 0);
        let br = Point::new(self.size-1, self.size-1);
        let _c = self.init_center(tl, br, &mut arr);
        arr
    }

    fn rand_height(&mut self) -> u8 {
        self.rng.gen_range::<u8>(0, self.height)
    }

    fn init(&mut self) -> Vec<u8> {
        let mut arr = vec![0; self.full_size];
        arr[0] = self.rand_height();
        arr[self.size-1] = self.rand_height();
        arr[self.full_size-1] = self.rand_height();
        arr[self.size*(self.size-1)] = self.rand_height();
        self.height /= 2;
        arr
    }

    fn init_center(&mut self, tl: Point, br: Point, arr: &mut Vec<u8>)
        -> Point {
        let corners = vec![
            tl, br,
            Point::new(tl.x, br.y),
            Point::new(br.x, tl.y)
        ];
        let mut avg = 0;
        for corner in &corners {
            avg += arr[corner.x + corner.y * self.size];
        }
        avg /= corners.len() as u8;
        let rand_val = avg + self.rand_height();
        let size = br - tl;
        let center = tl + size / 2;
        arr[center.x + center.y * self.size] =
            if rand_val <= self.height { 0 } else { rand_val - self.height};
        center
    }
}

#[test]
fn test_gen() {
    let size = 5;
    let height = 9;
    let map_id = 100;
    let mut ds = DiamondSquare::new(size, height, map_id);
    let map = ds.gen();
    assert_eq!(map.len(), size*size);
}
