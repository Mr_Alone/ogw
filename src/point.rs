use std::ops;

#[derive(Debug, Clone, Copy)]
pub struct Point {
    pub x: usize,
    pub y: usize,
}

impl Point {
    pub fn new(x: usize, y: usize) -> Point {
        Point {
            x: x,
            y: y,
        }
    }
}

impl ops::Mul<usize> for Point {
    type Output = Point;

    fn mul(self, other: usize) -> Point {
        Point {
            x: self.x * other,
            y: self.y * other,
        }
    }
}

impl ops::Div<usize> for Point {
    type Output = Point;

    fn div(self, other: usize) -> Point {
        Point {
            x: self.x / other,
            y: self.y / other,
        }
    }
}

impl ops::Add for Point {
    type Output = Point;

    fn add(self, other: Self) -> Point {
        Point {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl ops::Sub for Point {
    type Output = Point;

    fn sub(self, other: Self) -> Point {
        Point {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}
