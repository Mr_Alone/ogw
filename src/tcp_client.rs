use std::net::TcpStream;
use std::io::{Read, Write};
use std::str;

use session::Session;
use consts::{BUF_SIZE, EMPTY_MSG};

pub fn reqv_session(endpoint: &str, session_id: &str) -> Session {
    let mut stream = TcpStream::connect(endpoint).unwrap();
    stream.write(session_id.as_bytes()).unwrap();
    let mut buf = [0; BUF_SIZE];
    let size = stream.read(&mut buf).unwrap();
    let buf = str::from_utf8(&buf[..size]).unwrap();
    Session::parse(&buf)
}

pub fn reqv_session_with_friend(endpoint: &str, user_name: &str, friend_name: &str) -> Session {
    let session_id = format!("{};{}", user_name, friend_name);
    reqv_session(endpoint, &session_id)
}

pub fn reqv_stop(endpoint: &str) {
    let mut stream = TcpStream::connect(endpoint).unwrap();
    stream.write(EMPTY_MSG.as_bytes()).unwrap();
}
