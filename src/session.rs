use std::string::ToString;
use std::cmp::PartialEq;

use consts::SEPARATOR;

#[derive(Debug)]
pub struct Session {
    id: usize,
    map: usize,
}

impl Session {
    pub fn new(id: usize, map: usize) -> Session {
        Session {
            id: id,
            map: map
        }
    }

    pub fn parse(raw: &str) -> Session {
        let id_map: Vec<&str> = raw.split(SEPARATOR).collect();
        if id_map.len() != 2 {
            panic!("Invalid session data: {}", raw);
        }
        Session {
            id: id_map[0].parse().unwrap(),
            map:id_map[1].parse().unwrap(),
        }
    }
}

impl ToString for Session {
    fn to_string(&self) -> String {
        self.id.to_string() + SEPARATOR + &self.map.to_string()
    }
}

impl PartialEq for Session {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id && self.map == other.map
    }

    fn ne(&self, other: &Self) -> bool {
        !self.eq(other)
    }
}
