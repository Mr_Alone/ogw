use std::net::{SocketAddrV4, UdpSocket};
use std::str::FromStr;

use consts::{UDP_CLIENT_PORT, EMPTY_MSG};

pub fn reqv_stop(endpoint: &str) 
{
    let socket = UdpSocket::bind(("127.0.0.1", UDP_CLIENT_PORT)).unwrap();
    let connection = SocketAddrV4::from_str(endpoint).unwrap();
    socket.send_to(EMPTY_MSG.as_bytes(), &connection).unwrap();
}
