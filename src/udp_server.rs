use std::net::UdpSocket;
use std::result::Result;
use std::str;

use consts::BUF_SIZE;

fn handle_cmd(request: &str) -> Result<String, ()> {
    if request.is_empty() {
        return Err(())
    }
    Ok(request.to_string())
}

pub fn process(endpoint : &str) {
    let socket = UdpSocket::bind(endpoint).unwrap();
    let mut buf = [0; BUF_SIZE];
    loop {
        let (size, src) = socket.recv_from(&mut buf).unwrap();
        let buf = str::from_utf8(&buf[..size]).unwrap();
        match handle_cmd(&buf) {
            Ok(response) => socket.send_to(&response.as_bytes(), &src).unwrap(),
            Err(_) => break
        };
    }
}
