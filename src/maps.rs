use std::collections::HashMap;
use rand::{Rng, SeedableRng, StdRng};
use time;

pub struct Maps {
    maps: HashMap<usize, usize>,
}

impl Maps {

    pub fn new() -> Maps {
        Maps {
            maps: HashMap::new(),
        }
    }

    pub fn get_map(&mut self, session_id: &usize) -> usize {
        match self.maps.get(session_id) {
            Some(&map) => map,
            None => self.create_map(session_id)
        }
    }
    
    fn create_map(&mut self, session_id: &usize) -> usize {
        let ts = time::get_time().sec as usize;
        let seed: &[_] = &[ts, *session_id];
        let mut rng: StdRng = SeedableRng::from_seed(seed);
        rng.gen::<usize>()
    }
}
