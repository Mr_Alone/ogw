extern crate ogw;

use std::thread;
use std::thread::JoinHandle;

use ogw::{udp_server, udp_client, tcp_server, tcp_client};
use ogw::consts::{UDP_SERV_ADDR, TCP_SERV_ADDR, UDP_ENDPOINT, TCP_ENDPOINT};

fn start_servers(udp_addr: &'static str, tcp_addr: &'static str)
    -> (JoinHandle<()>, JoinHandle<()>) {
    let udp_thread = thread::spawn(move || {
        udp_server::process(udp_addr);
    });
    let tcp_thread = thread::spawn(move || {
        tcp_server::process(tcp_addr);
    });
    (udp_thread, tcp_thread)
}

fn main()
{
    let (udp_thread, tcp_thread) = start_servers(UDP_SERV_ADDR, TCP_SERV_ADDR);
    
    udp_client::reqv_stop(UDP_ENDPOINT);
    tcp_client::reqv_stop(TCP_ENDPOINT);

    udp_thread.join().unwrap();
    tcp_thread.join().unwrap();
}

#[test]
fn test_session() {
    let (udp_thread, tcp_thread) = start_servers(UDP_SERV_ADDR, TCP_SERV_ADDR);

    let black_session = tcp_client::reqv_session(TCP_ENDPOINT, "mr_black");
    let pink_session = tcp_client::reqv_session(TCP_ENDPOINT, "mr_pink");
    assert_ne!(black_session, pink_session);
    let session = tcp_client::reqv_session(TCP_ENDPOINT, "mr_black");
    assert_eq!(black_session, session);
    let session = tcp_client::reqv_session_with_friend(TCP_ENDPOINT, "mr_pink", "mr_black");
    assert_eq!(black_session, session);
    let session = tcp_client::reqv_session(TCP_ENDPOINT, "mr_pink");
    assert_ne!(pink_session, session);
    assert_eq!(black_session, session);
    
    udp_client::reqv_stop(UDP_ENDPOINT);
    tcp_client::reqv_stop(TCP_ENDPOINT);

    udp_thread.join().unwrap();
    tcp_thread.join().unwrap();
}
