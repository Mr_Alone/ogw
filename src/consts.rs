pub const UDP_SERV_ADDR: &'static str = "0.0.0.0:10100";
pub const TCP_SERV_ADDR: &'static str = "0.0.0.0:10101";
pub const UDP_ENDPOINT: &'static str = "127.0.0.1:10100";
pub const TCP_ENDPOINT: &'static str = "127.0.0.1:10101";
pub const UDP_CLIENT_PORT: u16 = 10102;

pub const EMPTY_MSG: &'static str = "";

pub const BUF_SIZE: usize = 1024;
pub const SEPARATOR: &'static str = ";";
