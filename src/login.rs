use std::collections::HashMap;

pub struct Login {
    sessions: HashMap<String, usize>,
}

impl Login {

    pub fn new() -> Login {
        Login {
            sessions: HashMap::new(),
        }
    }

    pub fn get_new_session(&mut self, user_name: &str) -> usize {
        match self.sessions.get(user_name) {
            Some(&session) => session,
            None => self.create_session(user_name)
        }
    }
    
    pub fn get_attached_session(&mut self, user_name: &str, friend_name: &str) -> usize {
        let session = match self.sessions.get(friend_name) {
            Some(&session) => session,
            None => self.create_session(friend_name)
        };
        self.attach_session(user_name, session);
        session
    }

    fn create_session(&mut self, user_name: &str) -> usize {
        let session = self.sessions.len() + 1;
        self.sessions.insert(user_name.to_string(), session);
        session
    }

    fn attach_session(&mut self, user_name: &str, session: usize) {
        self.sessions.insert(user_name.to_string(), session);
    }
}
